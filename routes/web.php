<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
// Route url

Route::get('#', function(){

})->name('none');

Route::get('/', 'DashboardController@dashboardAnalytics')->name('index');

Route::get('/sk-layout-fixed', 'StaterkitController@fixed_layout');

Route::resource('pegawai', 'PegawaiController');
Route::resource('divisi', 'DivisiController');
Route::resource('jabatan', 'JabatanController');
Route::resource('calon-pegawai', 'CalonPegawaiController');
Route::get('calon-pegawai/terima/{calonPegawai}', 'CalonPegawaiController@terima')->name('calon-pegawai.terima');
Route::get('calon-pegawai/tolak/{calonPegawai}', 'CalonPegawaiController@tolak')->name('calon-pegawai.tolak');

Route::resource('absensi', 'AbsensiController');
Route::resource('penggajian', 'PenggajianController');

Route::get('/laporan', 'PenggajianController@laporan')->name('laporan.index');
Route::post('/laporan/cetak', 'PenggajianController@cetakLaporan')->name('laporan.cetak');


