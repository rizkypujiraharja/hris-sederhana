<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGajisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gaji', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('absensi_id');
            $table->bigInteger('gaji_pokok');
            $table->bigInteger('jam_lembur');
            $table->bigInteger('rate_lembur');
            $table->bigInteger('rate_sppd');
            $table->bigInteger('uang_makan_transport');
            $table->bigInteger('rate_bolos');
            $table->bigInteger('gaji_total');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gajis');
    }
}
