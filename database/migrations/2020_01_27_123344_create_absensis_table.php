<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAbsensisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('absensi', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('pegawai_id');
            $table->string('periode');
            $table->integer('jml_hadir')->unsigned();
            $table->integer('jml_bolos')->unsigned();
            $table->integer('jml_izin')->unsigned();
            $table->integer('jml_cuti')->unsigned();
            $table->integer('jml_sppd')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('absensis');
    }
}
