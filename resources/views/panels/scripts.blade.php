    {{-- Vendor Scripts --}}
        <script src="{{ asset(mix('vendors/js/vendors.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/ui/prism.min.js')) }}"></script>

        <script src="{{ asset('vendors/js/extensions/sweetalert2.all.min.js') }}"></script>
        @yield('vendor-script')
        {{-- Theme Scripts --}}
        <script src="{{ asset(mix('js/core/app-menu.js')) }}"></script>
        <script src="{{ asset(mix('js/core/app.js')) }}"></script>
        <script src="{{ asset(mix('js/scripts/components.js')) }}"></script>
@if($configData['blankPage'] == false)
        <script src="{{ asset(mix('js/scripts/footer.js')) }}"></script>
@endif

<script>
@if(Session::has('alert-success'))
Swal.fire({
        title: "Berhasil !",
	text: "{{ Session::get('alert-success')}}",
        type: "success",
})
@endif

@if(Session::has('alert-error'))
Swal.fire({
        title: "Gagal !",
	text: "{{ Session::get('alert-error')}}",
        type: "error",
})
@endif
</script>