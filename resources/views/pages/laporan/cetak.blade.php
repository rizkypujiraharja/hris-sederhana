<style>
    .page-break {
        page-break-after: always;
    }
    .border{
        border: 1px solid black;
        border-style: outset;
        padding: 3px;
    }
</style>
<center>
<h2 style="line-height:3px;font-size:14pt">Laporan Gaji Periode {{ $periode }}</h2>
<h4 style="line-height:1px">PT. Barokah Kreasi Solusindo</h4>
</center>
<hr>

<table style="border-spacing: 0px;">
    <tr class="border">
        <td class="border"><span>No.</span></td>
        <td class="border"><span style="width:200px">Nama</span></td>
        <td class="border"><span style="width:140px">Divisi</span></td>
        <td class="border"><span style="width:140px">Jabatan</span></td>
        <td class="border"><span style="width:150px">Total Gaji</span></td>
    </tr>
    @foreach ($gaji as $key => $data)
        <tr class="border">
            <td class="border">{{ $key+1 }}</td>
            <td class="border">{{ $data->absensi->pegawai->name }}</td>
            <td class="border">{{ $data->absensi->pegawai->divisi->name }}</td>
            <td class="border">{{ $data->absensi->pegawai->jabatan->name }}</td>
            <td class="border">{{ $data->gaji_total }}</td>
        </tr>
    @endforeach
</table>