@extends('layouts/contentLayoutMaster')

@section('title', 'Divisi')

@section('vendor-style')
        <!-- vednor css files -->
        <link rel="stylesheet" href="{{ asset('vendors/css/pickers/pickadate/pickadate.css') }}">
@endsection

  @section('content')
    {{-- Dashboard Analytics Start --}}
    <section id="pegawai">
      <div class="card">
        <div class="card-header">
            <h4 class="card-title">Pilih Periode Laporan</h4>
        </div>
        <div class="card-content">
            <div class="card-body">
                <form class="form" action="{{ route('laporan.cetak') }}" method="POST">
                    @csrf
                    <div class="form-body">
                        <div class="row">
                            <div class="col-6 offset-3">
                                <div class="form-label-group">
                                    <select id="periode" class="form-control" name="bulan">
                                        <option value="Januari">Januari</option>
                                        <option value="Februari">Februari</option>
                                        <option value="Maret">Maret</option>
                                        <option value="April">April</option>
                                        <option value="Mei">Mei</option>
                                        <option value="Juni">Juni</option>
                                        <option value="Juli">Juli</option>
                                        <option value="Agustus">Agustus</option>
                                        <option value="September">September</option>
                                        <option value="Oktober">Oktober</option>
                                        <option value="November">November</option>
                                        <option value="Desember">Desember</option>
                                  </select>
                                </div>
                                <div class="form-label-group">
                                    <select id="periode" class="form-control" name="tahun">
                                        @for ($i = 2020; $i < 2025; $i++)
                                        <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                  </select>
                                </div>
                            </div>
                            <div class="col-6 offset-3">
                                <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Cetak</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </section>
  <!-- Dashboard Analytics end -->
  @endsection
