@extends('layouts/contentLayoutMaster')

@section('title', 'Penggajian')

@section('vendor-style')
        <!-- vednor css files -->
        <link rel="stylesheet" href="{{ asset('vendors/css/pickers/pickadate/pickadate.css') }}">
@endsection

  @section('content')
    {{-- Dashboard Analytics Start --}}
    <section id="pegawai">
      <div class="card">
        <div class="card-header">
            <h4 class="card-title">Penggajian Pegawai</h4>
        </div>
        <div class="card-content">
            <div class="card-body">
                <form class="form" action="{{ route('penggajian.store') }}" method="POST">
                    @csrf
                    <input type="hidden" name="penggajian_id" value="{{ $absensi->id }}">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-label-group">
                                    <input type="text" id="pegawai" class="form-control" placeholder="Jumlah hadir (hari)" value="{{ $absensi->pegawai->name }}" name="pegawai" disabled>
                                    <label for="pegawai">Nama Pegawai</label>
                                </div>
                            </div>

                            <div class="col-6">
                                <div class="form-label-group">
                                    <input type="text" id="periode" class="form-control" placeholder="Jumlah hadir (hari)" value="{{ $absensi->periode }}" name="periode" disabled>
                                    <label for="periode">Periode</label>
                                </div>
                            </div>


                            <div class="col-3">
                                <div class="form-label-group">
                                    <input type="text" id="pegawai" class="form-control" placeholder="Jumlah hadir (hari)" value="{{ $absensi->jml_hadir }}" name="jml_hadir" disabled>
                                    <label for="jumlah-hadir">Jumlah hadir (hari)</label>
                                </div>

                                <div class="form-label-group">
                                    <input type="text" id="jumlah-sakit" class="form-control" placeholder="Jumlah sakit (hari)" value="{{ $absensi->jml_sakit }}" name="jml_sakit" disabled>
                                    <label for="jumlah-sakit">Jumlah sakit (hari)</label>
                                </div>

                                <div class="form-label-group">
                                    <input type="text" id="jumlah-bolos" class="form-control" placeholder="Jumlah Bolos (hari)" value="{{ $absensi->jml_bolos }}" name="jml_bolos" disabled>
                                    <label for="jumlah-bolos">Jumlah Bolos (hari)</label>
                                </div>
                            </div>

                            <div class="col-3">
                                <div class="form-label-group">
                                    <input type="text" id="jumlah-izin" class="form-control" placeholder="Jumlah Izin (hari)" value="{{ $absensi->jml_izin }}" name="jml_izin" disabled>
                                    <label for="jumlah-izin">Jumlah Izin (hari)</label>
                                </div>

                                <div class="form-label-group">
                                    <input type="text" id="jumlah-cuti" class="form-control" placeholder="Jumlah Cuti (hari)" value="{{ $absensi->jml_cuti }}" name="jml_cuti" disabled>
                                    <label for="jumlah-cuti">Jumlah Cuti (hari)</label>
                                </div>

                                <div class="form-label-group">
                                    <input type="text" id="jumlah-sppd" class="form-control" placeholder="Jumlah SPPD (hari)" value="{{ $absensi->jml_sppd }}" name="jml_sppd" disabled>
                                    <label for="jumlah-sppd">Jumlah SPPD (hari)</label>
                                </div>
                            </div>


                            <div class="col-6">
                                <div class="form-label-group">
                                    <input type="text" id="jam-lembur" class="form-control" placeholder="Jumlah Jam Lembur" name="lembur">
                                    <label for="jam-lembur">Jumlah Jam Lembur</label>
                                </div>

                                <div class="form-label-group">
                                    <input type="text" value="300000" id="uang-makan" class="form-control" placeholder="Uang Makan & Transport" name="uang_makan">
                                    <label for="uang-makan">Uang Makan & Transport</label>
                                </div>
                                <div class="float-right">
                                    <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Submit</button>
                                    {{-- <button type="reset" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Reset</button> --}}
                                </div>

                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </section>
  <!-- Dashboard Analytics end -->
  @endsection

  @section('myscript')
      <script>
          $(function(){
            $("#periode").val("{{$absensi->periode}}");
          })
      </script>
  @endsection
