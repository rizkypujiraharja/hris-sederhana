<style>
    .page-break {
        page-break-after: always;
    }
</style>

<h2 style="line-height:3px;font-size:14pt">Slip Gaji Karyawan</h2>
<h4 style="line-height:1px">PT. Barokah Kreasi Solusindo</h4>
<table>
    <tr>
        <td>NIP</td>
        <td>: {{ $penggajian->absensi->pegawai->nip }}</td>
        <td><div style="width:70px;"> </div></td>
        <td>Divisi</td>
        <td>: {{ $penggajian->absensi->pegawai->divisi->name }}</td>
        <td><div style="width:70px;"> </div></td>
        <td>Periode</td>
        <td>: {{ $penggajian->absensi->periode }}</td>
    </tr>
    <tr>
        <td>Nama</td>
        <td>: {{ $penggajian->absensi->pegawai->name }}</td>
        <td></td>
        <td>Jabatan</td>
        <td>: {{ $penggajian->absensi->pegawai->jabatan->name }}</td>
        <td colspan="3"></td>
    </tr>
</table>
<hr>
<table>
    <tr>
        <td>
            <div style="width:190px">
                <b style="font-size:13pt">Data Absensi</b>
                <table>
                    <tr>
                        <td><span style="width:120px">Jumlah Hadir</span></td>
                        <td>{{ $penggajian->absensi->jml_hadir }}</td>
                    </tr>
                    <tr>
                        <td>Jumlah Bolos</td>
                        <td>{{ $penggajian->absensi->jml_bolos }}</td>
                    </tr>
                    <tr>
                        <td>Jumlah Sakit</td>
                        <td>{{ $penggajian->absensi->jml_sakit }}</td>
                    </tr>
                    <tr>
                        <td>Jumlah Izin</td>
                        <td>{{ $penggajian->absensi->jml_izin }}</td>
                    </tr>
                    <tr>
                        <td>Jumlah Cuti</td>
                        <td>{{ $penggajian->absensi->jml_cuti }}</td>
                    </tr>
                    <tr>
                        <td>Jumlah SPPD</td>
                        <td>{{ $penggajian->absensi->jml_sppd }}</td>
                    </tr>
                </table>
            </div>
        </td>
        <td>
            <div style="width:270px">
                <b style="font-size:13pt">Pendapatan</b>
                <table>
                    <tr>
                        <td><span style="width:180px">Gaji Pokok</span></td>
                        <td>{{ $penggajian->absensi->pegawai->gaji_pokok }}</td>
                    </tr>
                    <tr>
                        <td>Uang Makan/Transport</td>
                        <td>{{ $penggajian->absensi->uang_makan_transport }}</td>
                    </tr>
                    <tr>
                        <td>Lembur</td>
                        <td>{{ $penggajian->absensi->jam_lembur * $penggajian->rate_lembur }}</td>
                    </tr>
                    <tr>
                        <td>Tunjangan SPPD</td>
                        <td>{{ $penggajian->absensi->jml_sppd * $penggajian->rate_sppd }}</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td><strong>Total Pendapatan</strong></td>
                        <td>{{ $penggajian->gaji_pokok + ($penggajian->rate_lembur * $penggajian->jam_lembur) + ($penggajian->rate_sppd * $penggajian->absensi->jml_sppd) + $penggajian->uang_makan_transport }}</td>
                    </tr>
                </table>
            </div>
        </td>
        <td>
            <div style="width:230px">
                <b style="font-size:13pt">Potongan</b>
                <table>
                    <tr>
                        <td><span style="width:130px">Potongan Absen</span></td>
                        <td>{{ $penggajian->absensi->jml_bolos * $penggajian->rate_bolos }}</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td><strong>Total Potongan</strong></td>
                        <td>{{ $penggajian->absensi->jml_bolos * $penggajian->rate_bolos }}</td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
</table>
<br>
<p style="font-size:13pt;float:right"><b>Take Home Pay : {{ $penggajian->gaji_total }}</b></p>

