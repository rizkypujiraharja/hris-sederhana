@extends('layouts/contentLayoutMaster')

@section('title', 'Absensi Pegawai')

  @section('content')
    {{-- Dashboard Analytics Start --}}
    <section id="pegawai">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h4 class="mb-0">List Penggajian Bulanan</h4>
              </div>
            <div class="card-content">
              <div class="table-responsive mt-1">
                <table class="table table-hover-animation mb-0">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama</th>
                      <th>Periode</th>
                      <th>Divisi</th>
                      <th>Total Gaji</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse ($gaji as $data)
                    <tr>
                      <td>{{(($gaji->currentPage()-1) * $gaji->perPage()) + $loop->iteration}}</td>
                      <td>{{ $data->absensi->pegawai->name }}</td>
                      <td>{{ $data->absensi->periode }}</td>
                      <td>{{ $data->absensi->pegawai->divisi->name }}</td>
                      <td>Rp. {{ $data->gaji_total }}</td>
                      <td>
                        <a href="{{ route('penggajian.show', $data) }}" class="badge badge-sm badge-info"> <span class="fa fa-print"></span></a>
                      </td>
                    </tr>
                    @empty
                    <tr>
                      <td colspan="11" align="center">Tidak ada data</td>
                    </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  <!-- Dashboard Analytics end -->
  @endsection
