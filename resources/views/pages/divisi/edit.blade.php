@extends('layouts/contentLayoutMaster')

@section('title', 'Divisi')

@section('vendor-style')
        <!-- vednor css files -->
        <link rel="stylesheet" href="{{ asset('vendors/css/pickers/pickadate/pickadate.css') }}">
@endsection

  @section('content')
    {{-- Dashboard Analytics Start --}}
    <section id="pegawai">
      <div class="card">
        <div class="card-header">
            <h4 class="card-title">Edit Divisi</h4>
        </div>
        <div class="card-content">
            <div class="card-body">
                <form class="form" action="{{ route('divisi.update', $divisi) }}" method="POST">
                    @method('PUT')
                    @csrf
                    <div class="form-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-label-group">
                                    <input type="text" id="nama-divisi" class="form-control" placeholder="Nama Divisi" name="nama" value="{{ $divisi->name }}">
                                    <label for="nama-divisi">Nama Divisi</label>
                                </div>
                            </div>
                            <div class="col-12">
                                <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Submit</button>
                                <button type="reset" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Reset</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </section>
  @endsection
