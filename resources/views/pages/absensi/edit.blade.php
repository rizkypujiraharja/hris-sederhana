@extends('layouts/contentLayoutMaster')

@section('title', 'Absensi')

@section('vendor-style')
        <!-- vednor css files -->
        <link rel="stylesheet" href="{{ asset('vendors/css/pickers/pickadate/pickadate.css') }}">
@endsection

  @section('content')
    {{-- Dashboard Analytics Start --}}
    <section id="pegawai">
      <div class="card">
        <div class="card-header">
            <h4 class="card-title">Edit Absensi</h4>
        </div>
        <div class="card-content">
            <div class="card-body">
                <form class="form" action="{{ route('absensi.update', $absensi) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-body">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-label-group">
                                    <select id="pegawai" class="form-control" name="pegawai_id" disabled>
                                        @foreach ($pegawai as $item)
                                            <option value="{{ $item->id }}" {{ $absensi->pegawai_id == $item->id ? 'selected' : '' }}>{{ $item->name }}</option>
                                        @endforeach
                                      </select>
                                    <label for="pegawai">Nama Pegawai</label>
                                </div>
                            </div>

                            <div class="col-6">
                                <div class="form-label-group">
                                    <select id="periode" class="form-control" name="periode" disabled>
                                            <option value="Januari {{ date('Y') }}">Januari {{ date('Y') }}</option>
                                            <option value="Februari {{ date('Y') }}">Februari {{ date('Y') }}</option>
                                            <option value="Maret {{ date('Y') }}">Maret {{ date('Y') }}</option>
                                            <option value="April {{ date('Y') }}">April {{ date('Y') }}</option>
                                            <option value="Mei {{ date('Y') }}">Mei {{ date('Y') }}</option>
                                            <option value="Juni {{ date('Y') }}">Juni {{ date('Y') }}</option>
                                            <option value="Juli {{ date('Y') }}">Juli {{ date('Y') }}</option>
                                            <option value="Agustus {{ date('Y') }}">Agustus {{ date('Y') }}</option>
                                            <option value="September {{ date('Y') }}">September {{ date('Y') }}</option>
                                            <option value="Oktober {{ date('Y') }}">Oktober {{ date('Y') }}</option>
                                            <option value="November {{ date('Y') }}">November {{ date('Y') }}</option>
                                            <option value="Desember {{ date('Y') }}">Desember {{ date('Y') }}</option>
                                      </select>
                                    <label for="periode">Periode</label>
                                </div>
                            </div>


                            <div class="col-6">
                                <div class="form-label-group">
                                    <input type="text" id="jumlah-hadir" class="form-control" placeholder="Jumlah hadir (hari)" value="{{ $absensi->jml_hadir }}" name="jml_hadir">
                                    <label for="jumlah-hadir">Jumlah hadir (hari)</label>
                                </div>

                                <div class="form-label-group">
                                    <input type="text" id="jumlah-sakit" class="form-control" placeholder="Jumlah sakit (hari)" value="{{ $absensi->jml_sakit }}" name="jml_sakit">
                                    <label for="jumlah-sakit">Jumlah sakit (hari)</label>
                                </div>

                                <div class="form-label-group">
                                    <input type="text" id="jumlah-bolos" class="form-control" placeholder="Jumlah Bolos (hari)" value="{{ $absensi->jml_bolos }}" name="jml_bolos">
                                    <label for="jumlah-bolos">Jumlah Bolos (hari)</label>
                                </div>
                            </div>

                            <div class="col-6">
                                <div class="form-label-group">
                                    <input type="text" id="jumlah-izin" class="form-control" placeholder="Jumlah Izin (hari)" value="{{ $absensi->jml_izin }}" name="jml_izin">
                                    <label for="jumlah-izin">Jumlah Izin (hari)</label>
                                </div>

                                <div class="form-label-group">
                                    <input type="text" id="jumlah-cuti" class="form-control" placeholder="Jumlah Cuti (hari)" value="{{ $absensi->jml_cuti }}" name="jml_cuti">
                                    <label for="jumlah-cuti">Jumlah Cuti (hari)</label>
                                </div>

                                <div class="form-label-group">
                                    <input type="text" id="jumlah-sppd" class="form-control" placeholder="Jumlah SPPD (hari)" value="{{ $absensi->jml_sppd }}" name="jml_sppd">
                                    <label for="jumlah-sppd">Jumlah SPPD (hari)</label>
                                </div>
                            </div>
                            <div class="col-12">
                                <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Submit</button>
                                <button type="reset" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Reset</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </section>
  <!-- Dashboard Analytics end -->
  @endsection

  @section('myscript')
      <script>
          $(function(){
            $("#periode").val("{{$absensi->periode}}");
          })
      </script>
  @endsection
