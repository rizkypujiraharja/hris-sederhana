@extends('layouts/contentLayoutMaster')

@section('title', 'Absensi Pegawai')

  @section('content')
    {{-- Dashboard Analytics Start --}}
    <section id="pegawai">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h4 class="mb-0">List Absensi Bulanan </h4>
              <a href="{{ route('absensi.create') }}" class="btn btn-sm btn-success">Tambah Absensi Pegawai</a>
            </div>
            <div class="card-content">
              <div class="table-responsive mt-1">
                <table class="table table-hover-animation mb-0">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Status</th>
                      <th>Nama</th>
                      <th>Periode</th>
                      <th>Divisi</th>
                      <th>Hadir</th>
                      <th>Sakit</th>
                      <th>Bolos</th>
                      <th>Izin</th>
                      <th>Cuti</th>
                      <th>SPPD</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse ($absensi as $data)
                    <tr>
                      <td>{{(($absensi->currentPage()-1) * $absensi->perPage()) + $loop->iteration}}</td>
                      <td>
                        @if ($data->penggajian)
                        <i class="fa fa-circle font-small-3 text-success mr-50"></i> Sudah
                        @else
                          <i class="fa fa-circle font-small-3 text-danger mr-50"></i> Belum
                        @endif
                      </td>
                      <td>{{ $data->pegawai->name }}</td>
                      <td>{{ $data->periode }}</td>
                      <td>{{ $data->pegawai->divisi->name }}</td>
                      <td>{{ $data->jml_hadir }}</td>
                      <td>{{ $data->jml_sakit }}</td>
                      <td>{{ $data->jml_bolos }}</td>
                      <td>{{ $data->jml_izin }}</td>
                      <td>{{ $data->jml_cuti }}</td>
                      <td>{{ $data->jml_sppd }}</td>
                      <td>
                        @if (!$data->penggajian)
                          <a href="{{ route('absensi.edit', $data) }}" class="btn badge badge-sm badge-warning"> <span class="fa fa-edit"></span></a>
                          <span data-title="{{$data->pegawai->name}}" href="{{ route('absensi.destroy', $data) }}" class="btn badge badge-sm badge-danger btn-delete"> <span class="fa fa-trash"></span></span>
                          <a href="{{ route('penggajian.create') }}?penggajian_id={{$data->id}}" class="btn badge badge-sm badge-info"> <span class="fa fa-dollar"></span></a>
                        @else
                        -
                        @endif

                      </td>
                    </tr>
                    @empty
                    <tr>
                      <td colspan="11" align="center">Tidak ada data</td>
                    </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <form action="" method="POST" id="deleteForm">
      @csrf
      @method('DELETE')
      <input type="submit" style="display: none;">
  </form>
  @endsection

@section('myscript')

<script type="text/javascript">
  $('.btn-delete').on('click', function(){
      var href = $(this).attr('href');
      var title = $(this).data('title');

      Swal.fire({
          title: "Hapus data "+ title +" ?",
          text: "Data yang dihapus tidak dapat dikembalikan !",
          type: "warning",
          showCancelButton: !0,
          confirmButtonColor: "#3085d6",
          cancelButtonColor: "#d33",
          confirmButtonText: "Yes, delete it!",
          confirmButtonClass: "btn btn-primary",
          cancelButtonClass: "btn btn-danger ml-1",
          buttonsStyling: !1
      }).then(function(t) {
        if(t.value){
          $('#deleteForm').attr('action', href);
          $('#deleteForm').submit();
        }
      })
  });
</script>
@endsection

