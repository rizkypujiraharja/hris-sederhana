@extends('layouts/contentLayoutMaster')

@section('title', 'Pegawai')

  @section('content')
    {{-- Dashboard Analytics Start --}}
    <section id="pegawai">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h4 class="mb-0">List Pegawai</h4>
              <a href="{{ route('pegawai.create') }}" class="btn btn-sm btn-success">Tambah Pegawai</a>
            </div>
            <div class="card-content">
              <div class="table-responsive mt-1">
                <table class="table table-hover-animation mb-0">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>NIP</th>
                      <th>Nama</th>
                      <th>Divisi</th>
                      <th>Jabatan</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse ($pegawai as $data)
                    <tr>
                      <td>{{(($pegawai->currentPage()-1) * $pegawai->perPage()) + $loop->iteration}}</td>
                      <td>{{ $data->nip }}</td>
                      <td>{{ $data->name }}</td>
                      <td>{{ $data->divisi->name }}</td>
                      <td>{{ $data->jabatan->name }}</td>
                      <td>
                          {{-- <a href="" class="btn badge badge-sm badge-info"> <span class="fa fa-eye"></span></a> --}}
                          <a href="{{ route('pegawai.edit', $data) }}" class="btn badge badge-sm badge-warning"> <span class="fa fa-edit"></span></a>
                          <span data-title="{{$data->name}}" href="{{ route('pegawai.destroy', $data) }}" class="btn badge badge-sm badge-danger btn-delete"> <span class="fa fa-trash"></span></span>
                      </td>
                    </tr>
                    @empty
                    <tr>
                      <td colspan="6" align="center">Tidak ada data</td>
                    </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <form action="" method="POST" id="deleteForm">
      @csrf
      @method('DELETE')
      <input type="submit" style="display: none;">
  </form>
  @endsection

@section('myscript')

  <script type="text/javascript">
    $('.btn-delete').on('click', function(){
        var href = $(this).attr('href');
        var title = $(this).data('title');

        Swal.fire({
            title: "Hapus data "+ title +" ?",
            text: "Data yang dihapus tidak dapat dikembalikan !",
            type: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Yes, delete it!",
            confirmButtonClass: "btn btn-primary",
            cancelButtonClass: "btn btn-danger ml-1",
            buttonsStyling: !1
        }).then(function(t) {
          if(t.value){
            $('#deleteForm').attr('action', href);
            $('#deleteForm').submit();
          }
        })
    });
  </script>
@endsection

