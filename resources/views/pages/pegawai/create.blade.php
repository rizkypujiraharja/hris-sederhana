@extends('layouts/contentLayoutMaster')

@section('title', 'Pegawai')

@section('vendor-style')
        <!-- vednor css files -->
        <link rel="stylesheet" href="{{ asset('vendors/css/pickers/pickadate/pickadate.css') }}">
@endsection

  @section('content')
    {{-- Dashboard Analytics Start --}}
    <section id="pegawai">
      <div class="card">
        <div class="card-header">
            <h4 class="card-title">Tambah Pegawai</h4>
        </div>
        <div class="card-content">
            <div class="card-body">
                <form class="form" action="{{ route('pegawai.store') }}" method="POST">
                    @csrf
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-6 col-12">
                                <div class="form-label-group">
                                    <input type="text" id="nama-pegawai" class="form-control" placeholder="Nama Pegawai" name="nama">
                                    <label for="nama-pegawai">Nama Pegawai</label>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="form-label-group">
                                    <input type="text" id="nip" class="form-control" placeholder="N I P" name="nip">
                                    <label for="nip">N I P</label>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="form-label-group">
                                    <input type="text" id="tempat-lahir" class="form-control" placeholder="Tempat Lahir" name="tempat_lahir">
                                    <label for="tempat-lahir">Tempat Lahir</label>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="form-label-group">
                                    <input type="text" id="tanggal-lahir" class="form-control tanggal" name="tanggal_lahir" placeholder="Tanggal Lahir">
                                    <label for="tanggal-lahir">Tanggal Lahir</label>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="form-label-group">
                                    <input type="text" id="telepon" class="form-control" name="telepon" placeholder="No Telepon">
                                    <label for="telepon">No Telepon</label>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="form-label-group">
                                    <input type="email" id="email" class="form-control" name="email" placeholder="Email">
                                    <label for="email">Email</label>
                                </div>
                            </div>
                            <div class="col-12">
                              <div class="form-label-group">
                                  <textarea name="alamat" class="form-control" placeholder="Alamat Lengkap"></textarea>
                                  <label for="email">Alamat</label>
                              </div>
                            </div>

                            <div class="col-md-6 col-12">
                              <div class="form-label-group">
                                  <input type="text" id="tanggal-masuk" class="form-control tanggal" name="tanggal_masuk" placeholder="Tanggal Masuk">
                                  <label for="tanggal-masuk">Tanggal Masuk</label>
                              </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="form-label-group">
                                    <input type="text" id="gaji-pokok" class="form-control" name="gaji_pokok" placeholder="Gaji Pokok">
                                    <label for="gaji-pokok">Gaji Pokok</label>
                                </div>
                            </div>

                            <div class="col-md-6 col-12">
                                <div class="form-label-group">
                                    <select id="divisi" class="form-control" name="divisi">
                                        @foreach ($divisi as $item)
                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                              </div>
                              <div class="col-md-6 col-12">
                                  <div class="form-label-group">
                                      <select id="jabatan" class="form-control" name="jabatan">
                                        @foreach ($jabatan as $item)
                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                      </select>
                                  </div>
                                </div>
                            <div class="col-12">
                                <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Submit</button>
                                <button type="reset" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Reset</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </section>
  <!-- Dashboard Analytics end -->
  @endsection
  @section('vendor-script')
        <!-- vednor files -->
        <script src="{{ asset('vendors/js/pickers/pickadate/picker.js') }}"></script>
        <script src="{{ asset('vendors/js/pickers/pickadate/picker.date.js') }}"></script>
  @endsection

  @section('myscript')
      <script>
          $(function(){
            $(".tanggal").pickadate({
                selectYears: !0,
                selectMonths: !0,
                format: "dd mmmm yyyy",
                formatSubmit: "yyyy/mm/dd"
            })
          })
      </script>
  @endsection