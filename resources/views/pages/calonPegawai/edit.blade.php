@extends('layouts/contentLayoutMaster')

@section('title', 'Pegawai')

@section('vendor-style')
        <!-- vednor css files -->
        <link rel="stylesheet" href="{{ asset('vendors/css/pickers/pickadate/pickadate.css') }}">
@endsection

  @section('content')
    {{-- Dashboard Analytics Start --}}
    <section id="pegawai">
      <div class="card">
        <div class="card-header">
            <h4 class="card-title">Tambah Calon Pegawai</h4>
        </div>
        <div class="card-content">
            <div class="card-body">
                <form class="form" action="{{ route('calon-pegawai.update', $calonPegawai) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-6 col-12">
                                <div class="form-label-group">
                                    <input type="text" id="nama-pegawai" class="form-control" placeholder="Nama Pegawai" value="{{ $calonPegawai->name }}" name="nama">
                                    <label for="nama-pegawai">Nama Calon</label>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="form-label-group">
                                    <input type="text" id="pendidikan" class="form-control" placeholder="Pendidikan Terakhir" value="{{ $calonPegawai->pendidikan }}" name="pendidikan">
                                    <label for="pendidikan">Pendidikan Terakhir</label>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="form-label-group">
                                    <input type="text" id="tempat-lahir" class="form-control" placeholder="Tempat Lahir" value="{{ $calonPegawai->tempat_lahir }}" name="tempat_lahir">
                                    <label for="tempat-lahir">Tempat Lahir</label>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="form-label-group">
                                    <input type="text" id="tanggal-lahir" class="form-control tanggal" value="{{ $calonPegawai->tanggal_lahir }}" name="tanggal_lahir" placeholder="Tanggal Lahir">
                                    <label for="tanggal-lahir">Tanggal Lahir</label>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="form-label-group">
                                    <input type="text" id="telepon" class="form-control" value="{{ $calonPegawai->telepon }}" name="telepon" placeholder="No Telepon">
                                    <label for="telepon">No Telepon</label>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="form-label-group">
                                    <input type="email" id="email" class="form-control" value="{{ $calonPegawai->email }}" name="email" placeholder="Email">
                                    <label for="email">Email</label>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                              <div class="form-label-group">
                                  <textarea id="keahlian" name="alamat" class="form-control" placeholder="Alamat Lengkap">{{ $calonPegawai->alamat }}</textarea>
                                  <label for="alamat">Alamat</label>
                              </div>
                            </div>

                            <div class="col-md-6 col-12">
                                <div class="form-label-group">
                                    <textarea name="keahlian" class="form-control" placeholder="Keahlian">{{ $calonPegawai->keahlian }}</textarea>
                                    <label for="keahlian">Keahlian</label>
                                </div>
                              </div>

                            <div class="col-md-6 col-12">
                                <div class="form-label-group">
                                    <select id="divisi" class="form-control" name="divisi">
                                        @foreach ($divisi as $item)
                                            <option value="{{ $item->id }}" {{ $calonPegawai->divisi_id == $item->id ? 'selectd' : '' }}>{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="form-label-group">
                                    <input type="input" id="gaji" class="form-control" value="{{ $calonPegawai->permintaan_gaji }}" name="gaji" placeholder="Permintaan Gaji">
                                    <label for="gaji">Permintaan Gaji</label>
                                </div>
                            </div>
                            <div class="col-12">
                                <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Submit</button>
                                <button type="reset" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Reset</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </section>
  <!-- Dashboard Analytics end -->
  @endsection
  @section('vendor-script')
        <!-- vednor files -->
        <script src="{{ asset('vendors/js/pickers/pickadate/picker.js') }}"></script>
        <script src="{{ asset('vendors/js/pickers/pickadate/picker.date.js') }}"></script>
  @endsection

  @section('myscript')
      <script>
          $(function(){
            $(".tanggal").pickadate({
                selectYears: !0,
                selectMonths: !0,
                format: "dd mmmm yyyy",
                formatSubmit: "yyyy/mm/dd"
            })
          })
      </script>
  @endsection
