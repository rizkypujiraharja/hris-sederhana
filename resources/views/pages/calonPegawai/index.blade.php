@extends('layouts/contentLayoutMaster')

@section('title', 'Calon Pegawai')

  @section('content')
    {{-- Dashboard Analytics Start --}}
    <section id="pegawai">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h4 class="mb-0">List Calon Pegawai</h4>
              <a href="{{ route('calon-pegawai.create') }}" class="btn btn-sm btn-success">Tambah Calon Pegawai</a>
            </div>
            <div class="card-content">
              <div class="table-responsive mt-1">
                <table class="table table-hover-animation mb-0">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Status</th>
                      <th>Nama</th>
                      <th>Divisi</th>
                      <th>Permintaan Gaji</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse ($calonPegawai as $data)
                    <tr>
                      <td>{{(($calonPegawai->currentPage()-1) * $calonPegawai->perPage()) + $loop->iteration}}</td>
                      <td>
                        @switch($data->status)
                            @case(1)
                                <i class="fa fa-circle font-small-3 text-success mr-50"></i>Diterima
                                @break
                            @case(2)
                                <i class="fa fa-circle font-small-3 text-danger mr-50"></i>Ditolak

                                @break
                            @default
                            <i class="fa fa-circle font-small-3 text-warning mr-50"></i>Proses
                        @endswitch
                      </td>
                      <td>{{ $data->name }}</td>
                      <td>{{ $data->divisi->name }}</td>
                      <td>{{ $data->permintaan_gaji }}</td>
                      <td>
                        @if (!$data->status)
                        <a href="{{ route('calon-pegawai.terima', $data) }}" class="btn badge badge-sm badge-info"> <span class="fa fa-check"></span></a>
                        <a href="{{ route('calon-pegawai.tolak', $data) }}" class="btn badge badge-sm badge-secondary"> <span class="fa fa-close"></span></a>
                        @endif
                          <a href="{{ route('calon-pegawai.edit', $data) }}" class="btn badge badge-sm badge-warning"> <span class="fa fa-edit"></span></a>
                          <span data-title="{{$data->name}}" href="{{ route('calon-pegawai.destroy', $data) }}" class="btn badge badge-sm badge-danger btn-delete"> <span class="fa fa-trash"></span></span>
                      </td>
                    </tr>
                    @empty
                    <tr>
                      <td colspan="5" align="center">Tidak ada data</td>
                    </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>


    <form action="" method="POST" id="deleteForm">
      @csrf
      @method('DELETE')
      <input type="submit" style="display: none;">
  </form>
  @endsection

@section('myscript')

  <script type="text/javascript">
    $('.btn-delete').on('click', function(){
        var href = $(this).attr('href');
        var title = $(this).data('title');

        Swal.fire({
            title: "Hapus data "+ title +" ?",
            text: "Data yang dihapus tidak dapat dikembalikan !",
            type: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Yes, delete it!",
            confirmButtonClass: "btn btn-primary",
            cancelButtonClass: "btn btn-danger ml-1",
            buttonsStyling: !1
        }).then(function(t) {
          if(t.value){
            $('#deleteForm').attr('action', href);
            $('#deleteForm').submit();
          }
        })
    });
  </script>
@endsection


