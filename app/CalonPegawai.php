<?php

namespace App;

use Illuminate\Database\Eloquent\{Model, SoftDeletes};

class CalonPegawai extends Model
{
    use SoftDeletes;
    protected $table = 'pelamar';

    public function divisi()
    {
        return $this->belongsTo(Divisi::class);
    }
}
