<?php

namespace App;

use Illuminate\Database\Eloquent\{Model, SoftDeletes};

class Pegawai extends Model
{
    use SoftDeletes;
    protected $table = 'pegawai';

    public function divisi()
    {
        return $this->belongsTo(Divisi::class);
    }

    public function jabatan()
    {
        return $this->belongsTo(Jabatan::class);
    }
}
