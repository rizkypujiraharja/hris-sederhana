<?php

namespace App;

use Illuminate\Database\Eloquent\{Model, SoftDeletes};

class Jabatan extends Model
{
    use SoftDeletes;
    protected $table = 'jabatan';
}
