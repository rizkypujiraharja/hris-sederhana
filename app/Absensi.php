<?php

namespace App;

use Illuminate\Database\Eloquent\{Model, SoftDeletes};

class Absensi extends Model
{
    use SoftDeletes;
    protected $table = 'absensi';

    public function pegawai()
    {
        return $this->belongsTo(Pegawai::class);
    }

    public function penggajian()
    {
        return $this->hasOne(Gaji::class);
    }
}
