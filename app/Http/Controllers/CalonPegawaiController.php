<?php

namespace App\Http\Controllers;

use App\{CalonPegawai, Divisi};
use Illuminate\Http\Request;
use Carbon\Carbon;

class CalonPegawaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $calonPegawai = CalonPegawai::with('divisi')->paginate();
        return view('pages.calonPegawai.index', [
            'pageConfigs' => $this->pageConfigs,
            'calonPegawai' => $calonPegawai
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $divisi = Divisi::get();
        return view('pages.calonPegawai.create', [
            'pageConfigs' => $this->pageConfigs,
            'divisi' => $divisi
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $calonPegawai = new CalonPegawai;
        $calonPegawai->name = $request->nama;
        $calonPegawai->pendidikan = $request->pendidikan;
        $calonPegawai->tempat_lahir = $request->tempat_lahir;
        $calonPegawai->tanggal_lahir = Carbon::parse($request->tanggal_lahir);
        $calonPegawai->telepon = $request->telepon;
        $calonPegawai->email = $request->email;
        $calonPegawai->alamat = $request->alamat;
        $calonPegawai->keahlian = $request->keahlian;
        $calonPegawai->permintaan_gaji = $request->gaji;
        $calonPegawai->divisi_id = $request->divisi;
        $calonPegawai->save();

        return redirect()->route('calon-pegawai.index')->with('alert-success', 'Berhasil menambah calonPegawai');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CalonPegawai  $calonPegawai
     * @return \Illuminate\Http\Response
     */
    public function show(CalonPegawai $calonPegawai)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CalonPegawai  $calonPegawai
     * @return \Illuminate\Http\Response
     */
    public function edit(CalonPegawai $calonPegawai)
    {
        $divisi = Divisi::get();
        return view('pages.calonPegawai.edit', [
            'pageConfigs' => $this->pageConfigs,
            'calonPegawai' => $calonPegawai,
            'divisi' => $divisi
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CalonPegawai  $calonPegawai
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CalonPegawai $calonPegawai)
    {
        $calonPegawai->name = $request->nama;
        $calonPegawai->pendidikan = $request->pendidikan;
        $calonPegawai->tempat_lahir = $request->tempat_lahir;
        $calonPegawai->tanggal_lahir = Carbon::parse($request->tanggal_lahir);
        $calonPegawai->telepon = $request->telepon;
        $calonPegawai->email = $request->email;
        $calonPegawai->alamat = $request->alamat;
        $calonPegawai->keahlian = $request->keahlian;
        $calonPegawai->permintaan_gaji = $request->gaji;
        $calonPegawai->divisi_id = $request->divisi;
        $calonPegawai->save();

        return redirect()->route('calon-pegawai.index')->with('alert-success', 'Berhasil mengubah data calonPegawai');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CalonPegawai  $calonPegawai
     * @return \Illuminate\Http\Response
     */
    public function destroy(CalonPegawai $calonPegawai)
    {
        $calonPegawai->delete();

        return redirect()->route('calon-pegawai.index')->with('alert-success', 'Berhasil menghapus data calonPegawai');
    }

    public function terima(CalonPegawai $calonPegawai)
    {
        $calonPegawai->status = 1;
        $calonPegawai->save();

        return redirect()->route('calon-pegawai.index')->with('alert-success', 'Berhasil menerima calonPegawai');
    }

    public function tolak(CalonPegawai $calonPegawai)
    {
        $calonPegawai->status = 2;
        $calonPegawai->save();

        return redirect()->route('calon-pegawai.index')->with('alert-success', 'Berhasil menolak calonPegawai');
    }
}
