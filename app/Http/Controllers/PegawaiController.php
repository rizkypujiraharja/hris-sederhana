<?php

namespace App\Http\Controllers;

use App\{Pegawai, Divisi, Jabatan};
use Illuminate\Http\Request;
use Carbon\Carbon;

class PegawaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pegawai = Pegawai::with('divisi', 'jabatan')->paginate();
        return view('pages.pegawai.index', [
            'pageConfigs' => $this->pageConfigs,
            'pegawai' => $pegawai
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $divisi = Divisi::get();
        $jabatan = Jabatan::get();
        return view('pages.pegawai.create', [
            'pageConfigs' => $this->pageConfigs,
            'divisi' => $divisi,
            'jabatan' => $jabatan
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pegawai = new Pegawai;
        $pegawai->name = $request->nama;
        $pegawai->nip = $request->nip;
        $pegawai->tempat_lahir = $request->tempat_lahir;
        $pegawai->tanggal_lahir = Carbon::parse($request->tanggal_lahir);
        $pegawai->telepon = $request->telepon;
        $pegawai->email = $request->email;
        $pegawai->alamat = $request->alamat;
        $pegawai->tanggal_masuk = Carbon::parse($request->tanggal_masuk);
        $pegawai->gaji_pokok = $request->gaji_pokok;
        $pegawai->divisi_id = $request->divisi;
        $pegawai->jabatan_id = $request->jabatan;
        $pegawai->save();

        return redirect()->route('pegawai.index')->with('alert-success', 'Berhasil menambah pegawai');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pegawai  $pegawai
     * @return \Illuminate\Http\Response
     */
    public function show(Pegawai $pegawai)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pegawai  $pegawai
     * @return \Illuminate\Http\Response
     */
    public function edit(Pegawai $pegawai)
    {
        $divisi = Divisi::get();
        $jabatan = Jabatan::get();
        return view('pages.pegawai.edit', [
            'pageConfigs' => $this->pageConfigs,
            'pegawai' => $pegawai,
            'divisi' => $divisi,
            'jabatan' => $jabatan
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pegawai  $pegawai
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pegawai $pegawai)
    {
        $pegawai->name = $request->nama;
        $pegawai->nip = $request->nip;
        $pegawai->tempat_lahir = $request->tempat_lahir;
        $pegawai->tanggal_lahir = Carbon::parse($request->tanggal_lahir);
        $pegawai->telepon = $request->telepon;
        $pegawai->email = $request->email;
        $pegawai->alamat = $request->alamat;
        $pegawai->tanggal_masuk = Carbon::parse($request->tanggal_masuk);
        $pegawai->gaji_pokok = $request->gaji_pokok;
        $pegawai->divisi_id = $request->divisi;
        $pegawai->jabatan_id = $request->jabatan;
        $pegawai->save();

        return redirect()->route('pegawai.index')->with('alert-success', 'Berhasil mengubah data pegawai');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pegawai  $pegawai
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pegawai $pegawai)
    {
        $pegawai->delete();

        return redirect()->route('pegawai.index')->with('alert-success', 'Berhasil menghapus data pegawai');
    }
}
