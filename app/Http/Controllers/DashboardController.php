<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function dashboardAnalytics(){
        return view('/pages/dashboard-analytics', [
            'pageConfigs' => $this->pageConfigs
        ]);
    }

}

