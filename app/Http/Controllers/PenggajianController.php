<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{Absensi, Gaji};
use Barryvdh\DomPDF\Facade as PDF;

class PenggajianController extends Controller
{
    public function index()
    {
        $gaji = Gaji::with('absensi')->paginate();
        return view('pages.penggajian.index', [
            'pageConfigs' => $this->pageConfigs,
            'gaji' => $gaji
        ]);
    }

    public function create(Request $request)
    {
        $absensiId = $request->penggajian_id;
        $absensi = Absensi::with('pegawai')->find($absensiId);

        return view('pages.penggajian.create', [
            'pageConfigs' => $this->pageConfigs,
            'absensi' => $absensi
        ]);
    }

    public function store(Request $request)
    {
        $absensiId = $request->penggajian_id;
        $absensi = Absensi::with('pegawai')->find($absensiId);

        $gaji = new Gaji;
        $gaji->absensi_id = $absensi->id;
        $gaji->gaji_pokok = $absensi->pegawai->gaji_pokok;
        $gaji->jam_lembur = $request->lembur;
        $gaji->rate_lembur = 20000;
        $gaji->rate_sppd = 150000;
        $gaji->uang_makan_transport = $request->uang_makan;
        $gaji->rate_bolos = 100000;
        $gaji->gaji_total = $gaji->gaji_pokok + ($gaji->rate_lembur * $gaji->jam_lembur) + ($gaji->rate_sppd * $absensi->jml_sppd) + $gaji->uang_makan_transport;
        $gaji->gaji_total = $gaji->gaji_total - ( $absensi->jml_bolos * $gaji->rate_bolos );
        $gaji->save();

        return redirect()->route('penggajian.index')->with('alert-success', 'Berhasil membuat slip gaji');
    }

    public function show(Gaji $penggajian)
    {
        $customPaper = array(0,0,587.00,348.80);
        $pdf = PDF::loadView('pages.penggajian.slip', compact('penggajian'))
                ->setPaper($customPaper, 'portrait');
        return $pdf->stream();
    }

    public function laporan(Request $request)
    {
        return view('pages.laporan.index', [
            'pageConfigs' => $this->pageConfigs
        ]);
    }

    public function cetakLaporan(Request $request)
    {
        $periode = $request->bulan . ' ' . $request->tahun;
        $gaji = Gaji::with('absensi')
                ->join('absensi', 'absensi.id', '=', 'gaji.absensi_id')
                ->where('absensi.periode', $periode)
                ->get();

        if(!count($gaji)){
            return redirect()->back()->with('alert-error', 'Data tidak ditemukan');
        }

        $pdf = PDF::loadView('pages.laporan.cetak', compact('gaji', 'periode'));
        return $pdf->stream();
    }
}
