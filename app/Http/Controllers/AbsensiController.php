<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{Absensi, Pegawai};

class AbsensiController extends Controller
{
    public function index()
    {
        $absensi = Absensi::with('pegawai', 'penggajian')->get();

        return view('pages.absensi.index', [
            'pageConfigs' => $this->pageConfigs,
            'absensi' => $absensi
        ]);
    }

    public function create(Request $request)
    {
        $pegawai = Pegawai::get();
        return view('pages.absensi.create', [
            'pageConfigs' => $this->pageConfigs,
            'pegawai' => $pegawai
        ]);
    }

    public function store(Request $request)
    {
        $cek = Absensi::where('pegawai_id', $request->pegawai_id)
                        ->where('periode', $request->periode)
                        ->count();
        if($cek){
            return redirect()->back()->with('alert-error', 'Data absensi pegawai ini sudah pernah dimasukkan');
        }

        $absensi = new Absensi;
        $absensi->pegawai_id = $request->pegawai_id;
        $absensi->periode = $request->periode;
        $absensi->jml_hadir = $request->jml_hadir;
        $absensi->jml_bolos = $request->jml_bolos;
        $absensi->jml_izin = $request->jml_izin;
        $absensi->jml_sakit = $request->jml_sakit;
        $absensi->jml_cuti = $request->jml_cuti;
        $absensi->jml_sppd = $request->jml_sppd;
        $absensi->save();

        return redirect()->route('absensi.index')->with('alert-success', 'Berhasil menambah data absensi');
    }

    public function edit(Absensi $absensi)
    {
        $pegawai = Pegawai::get();
        return view('pages.absensi.edit', [
            'pageConfigs' => $this->pageConfigs,
            'pegawai' => $pegawai,
            'absensi' => $absensi
        ]);
    }

    public function update(Request $request, Absensi $absensi)
    {
        $cek = Absensi::where('pegawai_id', $request->pegawai_id)
                        ->where('periode', $request->periode)
                        ->where('id', '!=', $absensi->id)
                        ->count();
        if($cek){
            return redirect()->back()->with('alert-error', 'Data absensi pegawai ini sudah pernah dimasukkan');
        }

        $absensi->jml_hadir = $request->jml_hadir;
        $absensi->jml_bolos = $request->jml_bolos;
        $absensi->jml_izin = $request->jml_izin;
        $absensi->jml_sakit = $request->jml_sakit;
        $absensi->jml_cuti = $request->jml_cuti;
        $absensi->jml_sppd = $request->jml_sppd;
        $absensi->save();

        return redirect()->route('absensi.index')->with('alert-success', 'Berhasil mengubah data absensi');
    }

    public function destroy(Absensi $absensi)
    {
        $absensi->delete();

        return redirect()->route('absensi.index')->with('alert-success', 'Berhasil menghapus data absensi');
    }
}
