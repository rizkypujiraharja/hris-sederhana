<?php

namespace App;

use Illuminate\Database\Eloquent\{Model, SoftDeletes};

class Divisi extends Model
{
    use SoftDeletes;
    protected $table = 'divisi';
}
