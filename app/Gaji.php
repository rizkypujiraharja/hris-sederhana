<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gaji extends Model
{
    protected $table = 'gaji';

    public function absensi()
    {
        return $this->belongsTo(Absensi::class);
    }
}
